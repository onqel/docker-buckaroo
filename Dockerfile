FROM archlinux/base
RUN pacman -Sy && yes | pacman -S wget git openssl-1.0

WORKDIR /apps
RUN wget https://github.com/LoopPerfect/buckaroo/releases/download/v2.2.0/buckaroo-linux -O buckaroo
RUN chmod +x buckaroo
RUN DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1 /apps/buckaroo
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
ENV PATH="/apps:${PATH}"
